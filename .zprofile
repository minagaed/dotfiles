# xdg standard configuration location (needed by some applications)
export XDG_CONFIG_HOME="$HOME/.config"

# add ~/.local/bin to the PATH (application executables)
export PATH="$HOME/.local/bin:$PATH"

# automatically run startx when logging in on tty1
[ -z "$DISPLAY" ] && [ $XDG_VTNR -eq 1 ] && startx
